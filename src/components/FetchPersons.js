import React, { Component } from "react";
import './FetchPersons.css'
import '../App.css'
import Moment from 'react-moment'
import { FaArrowDown } from 'react-icons/fa';


export default class FetchPersons extends Component {
    constructor(props){
        super(props)
        this.state = {
            error: null,
            loading: true,
            persons: [],
        };

        // this.showMore = this.showMore.bind(this);
    }

    // showMore() {
    //     this.setState({  })
    // }

    async componentDidMount() {
        // Using the Person Api from the url provided to fetch data
        await fetch("https://literate-tribble.vrmn17v8c56d2.us-west-2.cs.amazonlightsail.com/persons/")
        .then( response => response.json())
        .then(
            // handle the result
            (data) => {
                this.setState({
                    loading: false,
                    persons: data
                });
            },


            // Handle error
            (error) => {
                this.setState({
                    loading: false,
                    error
                })
            },
        )
        // const url = "https://literate-tribble.vrmn17v8c56d2.us-west-2.cs.amazonlightsail.com/persons/";
        // const response = await fetch(url);
        // const data = await response.json()
        // console.log(data)
        // this.setState({person: data[0], loading: false });
    }
    
    render() {

        const {error, loading, persons} = this.state;
        if(error) {
            return <div>Error in Loading</div>
        }else if(loading) {
            return <div>Loading  ...</div>
        }else{
            return (
                <div className="App">
                <h1>List of persons</h1>
                <h2>Fetch a list from the API and display it</h2>

                    <ol className="persons">
                        {
                            persons.map(person => (
                                <div className="person">
                                <li key={person.id} align="left">
                                    <div>
                                        <h2>👨 Name : {person.name}</h2>
                                        <p>📖 Biography : {person.biography}
                                        <button
                                        onClick={() => this.props.showMore(person)}>
                                        <FaArrowDown/>
                                        </button>
                                        </p>
                                        <p>⏰ Birth Date :
                                        <Moment 
                                        date={person.date_of_birth}
                                        parse="YYYY-MM-DD"
                                        format = " MMM Do Y"
                                        />
                                        </p>
                                        <div>
                                            <p>Tags : {person.metadata.tags[0]}, {person.metadata.tags[1]}, {person.metadata.tags[2]}</p>
                                        </div>
                                    </div>
                                </li>
                                </div>
                            ))
                        }
                    </ol>
                </div>
            );
        }
    }
}
